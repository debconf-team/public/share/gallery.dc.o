The gallery.dc.o repository is an archive of the contents of
https://gallery.debconf.org/, before it was shut down.

## Web

All files in the repository can be accessed via the web:
<https://salsa.debian.org/debconf-team/public/share/gallery.dc.o>

## Setup

The repository uses git LFS (git large file system).
This makes handling of big files more efficient, but requires some
additional attention when adding files to the repository.

1. You need the package `git-lfs`.
1. On Debian systems `# apt install git-lfs` (Buster and later).
1. Run `git lfs install` to set up the "lfs" filter in your system-wide
   git config.

Using `git clone` by itself will download all the large git LFS files
inside the repo, which will be many gigabytes.

Instead, you can do a shallow clone:

1. `$ GIT_LFS_SKIP_SMUDGE=1 git clone --config filter.lfs.smudge=true  git@salsa.debian.org:debconf-team/public/share/gallery.dc.o.git`
1. `$ cd gallery.dc.o`
1. Using smudge causes files to only be stored as references.
   To actually fetch a file, run `git lfs fetch -I <filename>`.
1. Use the repo as though it were a normal git repository.
   To add files, see below.

## Adding files

Git directs files to LFS, rather than the git repository, when the
filename matches a pattern in the `.gitattributes` file.
This repo is set up to store a number of common large files in LFS.

You can see the files already stored in LFS, by running
`git lfs ls-files`.

Before committing, run `git lfs status` to check that your large files
will be committed to LFS:

```
$ git add DSC_0001.JPG
$ git add VIDEO_0001.MP4
$ git lfs status
On branch master

Git LFS objects to be committed:

	DSC_0001.JPG (LFS: 0eb9017)
	VIDEO_0001.MP4 (Git: a9fe88d)

Git LFS objects not staged for commit:
```

In the above example, `DSC_0001.JPG` will be stored in LFS, but
`VIDEO_0001.MP4` will not.
To fix that, because it is reasonable to expect all MP4 files to be
stored in LFS:

```
$ git lfs track '*.MP4'
Tracking "*.MP4"
$ git add VIDEO_0001.MP4  # Re-adding it, this time to LFS
$ git add .gitattributes
$ git lfs status
On branch master

Git LFS objects to be committed:

	.gitattributes (Git: b8183ad -> Git: e280251)
	DSC_0001.JPG (LFS: 0eb9017)
	VIDEO_0001.MP4 (LFS: a9fe88d)

Git LFS objects not staged for commit:

```

Then, commit and push as usual:

    $ git add slides/testfile.svg
    $ git commit -m"commit message"
    $ git push origin master

## License

We assume the content was intended to be shared, but don't have
explicit licenses from the copyright owners.
